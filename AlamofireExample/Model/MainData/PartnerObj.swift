//
//  PartnerObj.swift
//  AlamofireExample
//
//  Created by home on 18.03.2018.
//  Copyright © 2018 Valodya Galstyan. All rights reserved.
//

import UIKit
import EVReflection

class PartnerObj: EVObject {
    var id = ""
    var img = ""
    var lang = ""
    var link = ""
    var pos = ""
    var status = ""
    var title = ""
    var uid = ""
}
