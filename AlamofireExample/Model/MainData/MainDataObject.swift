//
//  MainDataObject.swift
//  AlamofireExample
//
//  Created by home on 18.03.2018.
//  Copyright © 2018 Valodya Galstyan. All rights reserved.
//

import UIKit
import EVReflection

class MainDataObject: EVObject {
    var success = ""
    var congress = Congress()
    var partners = [PartnerObj]()
    var errors = [String]()
    var errorCode = 0

}
