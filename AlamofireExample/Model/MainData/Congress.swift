//
//  Congress.swift
//  AlamofireExample
//
//  Created by home on 18.03.2018.
//  Copyright © 2018 Valodya Galstyan. All rights reserved.
//

import UIKit
import EVReflection

class Congress: EVObject {
    var cover_image = ""
    var date_from = ""
    var date_to = ""
    var title = ""
    var img = ""
    var url = ""
    var sub_title = ""
    
}
