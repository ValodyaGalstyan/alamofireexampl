//
//  ConstantsManager.swift
//  AlamofireExample
//
//  Created by Khachatur Badalyan on 9/6/18.
//  Copyright © 2018 Valodya Galstyan. All rights reserved.
//

import UIKit
import Foundation

struct URLDefines {
	// http://dev-tereos.synten.com   dev
	static let API_BASE_URL = "http://dev-tereos.synten.com"
}


extension UITextField {
	func setLeftPaddingPoints(_ amount:CGFloat){
		let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
		self.leftView = paddingView
		self.leftViewMode = .always
	}
	func setRightPaddingPoints(_ amount:CGFloat) {
		let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
		self.rightView = paddingView
		self.rightViewMode = .always
	}
}
