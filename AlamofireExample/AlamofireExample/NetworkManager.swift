//
//  NetworkManager.swift
//  AlamofireExample
//
//  Created by Khachatur Badalyan on 9/6/18.
//  Copyright © 2018 Valodya Galstyan. All rights reserved.
//

import UIKit
import Alamofire

class NetworkManager: NSObject {


	static let sharedInstance = NetworkManager()

	override init() {

	}

	func loginRequest(url:String, method:HTTPMethod, parameters:Dictionary<String, Any>?,  success: @escaping (_ respons:Any?, _ statusCode:Int) -> Void , failure: @escaping (_ statusCode:Int) -> Void) {

		let headers = ["Accept": "application/json"]




			Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (respons) in


				if respons.result.isSuccess {
					success(respons.result.value,(respons.response?.statusCode)!)
				}else if respons.result.isFailure {
					failure(401)
				}
			}



	}
    func POSTRequest(url: String, parameters: Dictionary<String, Any>? , success : @escaping (( _ _response: Any?,_ statusCode: Int)->(Void)), failure: @escaping ((_ errorCode: Int)->(Void))) {
        
        let headers = ["Accept": "application/json"]
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            
            if response.result.isSuccess {
                success(response.result.value,(response.response?.statusCode)!)
            } else if response.result.isFailure {
                failure(402)
            }
        }
        
    }
    
    func GETRequest(url: String, parameters: Dictionary<String, Any>? , success : @escaping (( _ _response: Any?,_ statusCode: Int)->(Void)), failure: @escaping ((_ errorCode: Int)->(Void))) {
        
        let headers = ["Accept": "application/json"]
        
        Alamofire.request(url, method: .get, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            
            if response.result.isSuccess {
                success(response.result.value,(response.response?.statusCode)!)
            } else if response.result.isFailure {
                failure(402)
            }
        }
        
    }
}
