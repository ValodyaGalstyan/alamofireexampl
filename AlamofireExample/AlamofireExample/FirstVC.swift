//
//  FirstVC.swift
//  AlamofireExample
//
//  Created by home on 18.03.2018.
//  Copyright © 2018 Valodya Galstyan. All rights reserved.
//

import UIKit
import DKImagePickerController

class FirstVC: UIViewController {
    
    var accetss : [DKAsset]?

    @IBOutlet weak var myImageView: UIImageView!
    
    @IBOutlet weak var addButton: UIButton!
    
    @IBOutlet weak var sendButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    func openCamer() {
        let pickerController = DKImagePickerController()
        pickerController.sourceType = .both
        pickerController.defaultSelectedAssets = self.accetss
        self.present(pickerController, animated: true, completion: nil)
        
        pickerController.didSelectAssets = { [weak self ] (assets : [DKAsset]) in
            print("didSelectAccets")
            self?.accetss?.removeAll()
            self?.accetss = assets
            for asset in assets {
                asset.fetchFullScreenImageWithCompleteBlock{(image, info) in
                self?.myImageView.image = image
            }
            
        }
    }

    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func addAction(_ sender: UIButton) {
        self.openCamer()
    }
    
    @IBAction func sendAction(_ sender: UIButton) {
    }
    
    
}
