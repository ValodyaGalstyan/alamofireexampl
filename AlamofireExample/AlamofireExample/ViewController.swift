//
//  ViewController.swift
//  AlamofireExample
//
//  Created by Khachatur Badalyan on 9/6/18.
//  Copyright © 2018 Valodya Galstyan. All rights reserved.
//



// Login   :   test@mail.ru
// Pass    :   123456

import UIKit
import Alamofire

class ViewController: BaseViewController,UITextFieldDelegate {

	@IBOutlet weak var userEmailTF: UITextField!
	@IBOutlet weak var userPasswordrTF: UITextField!
	@IBOutlet weak var loginButton: UIButton!
	@IBOutlet weak var chackboxImageView: UIImageView!

	var isChecked = false
	var stayLogined = "0"

   @IBOutlet weak var scrollView: UIScrollView!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.userEmailTF.delegate = self
		self.userPasswordrTF.delegate = self

		NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)

		self.loginButton.layer.cornerRadius = self.loginButton.frame.size.height / 2
		self.loginButton.layer.borderWidth = 1
		self.loginButton.layer.borderColor = UIColor.white.cgColor
		self.loginButton.layer.masksToBounds = true
		self.userEmailTF.setLeftPaddingPoints(10)
		self.userPasswordrTF.setLeftPaddingPoints(10)
		self.userEmailTF.placeholder = "Email"
		self.userPasswordrTF.placeholder = "Password"

	}

	public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		if textField == self.userEmailTF {
			self.userEmailTF.resignFirstResponder()
			self.userPasswordrTF.becomeFirstResponder()
		} else {
			self.userPasswordrTF.resignFirstResponder()
		}
		return true
	}
	
	@IBAction func chackboxBtnAction(_ sender: UIButton) {

		self.userEmailTF.resignFirstResponder()
		self.userPasswordrTF.resignFirstResponder()

		if !self.isChecked {
			self.chackboxImageView.image = UIImage(named: "checked")
			self.isChecked = true
			self.stayLogined = "1"
		}else {
			self.chackboxImageView.image = UIImage(named: "nonChecked")
			self.isChecked = false
			self.stayLogined = "0"
		}


	}

	@IBAction func jeCommandeBtnAction(_ sender: UIButton) {
		self.userEmailTF.resignFirstResponder()
		self.userPasswordrTF.resignFirstResponder()

		if (self.userEmailTF.text?.count)! > 0 && (self.userPasswordrTF.text?.count)! > 0 {
			
			if (self.reachabilityMnager?.isReachable)! {

				print("Network is OK")
				let params:[String : Any] = [
					"email":(self.userPasswordrTF.text)! as String,
					"password":(self.userEmailTF.text)! as String,
					"device_token":UIDevice.current.identifierForVendor!.uuidString,
					"stay_logged_in":self.stayLogined
				]

				let requestAPI =  URLDefines.API_BASE_URL + "/app/auth/" + UIDevice.current.identifierForVendor!.uuidString
				print(requestAPI)


				NetworkManager.sharedInstance.loginRequest(url: requestAPI, method: .post, parameters: params, success: { respons,statusCode in


					let JSON = respons as? NSDictionary
					if statusCode == 200 {

						let result = JSON?["result"]! as? NSDictionary
						let message = JSON?["success_message"]! as! String
						let userId = result?["id"] as! String

						print("message = ",message)
						print("userId = ",userId)
					} else if statusCode == 203 {
						let result = JSON?["errors"]! as? NSDictionary

						print("result = ",result)
					}

				}) { errorCode in

				}

			} else {
				print("No Internet")
				self.showAlert(message: "Internet connection lost", title: "")
			}

		} else {
			self.showAlert(message: "All fields are required", title: "")
		}

	}
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}

	@objc func keyboardWillShow(_ sender:NSNotification) -> () {

		let height:CGFloat = (sender.userInfo![UIKeyboardFrameEndUserInfoKey] as! CGRect).size.height
		let edgeInsets:UIEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: (height + 30), right: 0)
		self.scrollView.contentInset = edgeInsets
		self.scrollView.scrollIndicatorInsets = edgeInsets
	}

	@objc func keyboardWillHide(_ sender:NSNotification) -> () {
		let edgeInsets:UIEdgeInsets = .zero
		self.scrollView.contentInset = edgeInsets
		self.scrollView.scrollIndicatorInsets = edgeInsets
	}
    
    @IBAction func myBtn(_ sender: UIButton) {
        performSegue(withIdentifier: "from", sender: sender)
    }
    
}

