//
//  BaseViewController.swift
//  AlamofireExample
//
//  Created by Khachatur Badalyan on 9/6/18.
//  Copyright © 2018 Valodya Galstyan. All rights reserved.
//

import UIKit
import Alamofire

class BaseViewController: UIViewController {
    
// reachablility -n sarqum enq interneti arkayutyun@ stugelu hamar e
    // urdex uzenanq zapros annenq stugum enq .. if seld.rechabltiManager.isRechabld {}   
    
	var reachabilityMnager: NetworkReachabilityManager? = nil

    override func viewDidLoad() {
        super.viewDidLoad()

		self.reachabilityMnager = NetworkReachabilityManager(host: "www.apple.com")

		print(reachabilityMnager!.isReachable)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

	func showAlert(message:String,title:String) {
		let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)

		let okAction = UIAlertAction(title: "OK", style: .default)


		alertController.addAction(okAction)

		present(alertController, animated: true, completion: nil)
	}

}
