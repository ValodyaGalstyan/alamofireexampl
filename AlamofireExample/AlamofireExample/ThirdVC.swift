//
//  ThirdVC.swift
//  AlamofireExample
//
//  Created by home on 18.03.2018.
//  Copyright © 2018 Valodya Galstyan. All rights reserved.
//

imimport UIKit
import WebKit

class WebViewController: UIViewController, WKNavigationDelegate {
    var link = ""
    var myWebView = WKWebView()
    
    override func viewDidLoad() {
        self.myWebView.navigationDelegate = self
        
        self.myWebView.frame = self.view.frame(forAlignmentRect: CGRect(x: 0.0, y: 80.0, width: self.view.frame.width, height: self.view.frame.height))
        self.view.addSubview(self.myWebView)
        if link.count > 0 {
            self.myWebView.load(URLRequest(url: URL(string: link)!))
        }
    }
}

   
   


