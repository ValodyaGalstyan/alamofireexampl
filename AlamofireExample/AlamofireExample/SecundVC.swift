//
//  SecundVC.swift
//  AlamofireExample
//
//  Created by home on 18.03.2018.
//  Copyright © 2018 Valodya Galstyan. All rights reserved.
//

import UIKit
import Alamofire
import MBProgressHUD
import AlamofireImage

class SecundVC: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        getMainData()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getMainData() {
        let params = ["lang" : "en"]
        let requestAPI = "http://fiabci.org/api/main/index/ad6300cad547ec061d56476829c5459ea87df927"
        if (self.reachabilityMnager?.isReachable)! {
            NetworkManager.sharedInstance.POSTRequest(url: requestAPI , parameters: params, success: { (responce, statusCode) -> (Void) in
                let JSONDictionary = responce as! NSDictionary
                MainData.sharedInstance = MainData(dictionary: JSONDictionary)
                if MainData.sharedInstance.result[0].success == "1" {
                    //print(MainData.sharedInstance.result[0])
                    if MainData.sharedInstance.result[0].partners.count > 0 {
                        self.tableView.reloadData()
                        self.tableView.isHidden = false
                        //reload tb
                        
                    }
                }
            }) { (errorCode) -> (Void) in
                
            }

        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if MainData.sharedInstance.result.count > 0 {
                self.tableView.isHidden = false
                return MainData.sharedInstance.result[0].partners.count
                }  else {
                self.tableView.isHidden = true
                return 1
    }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! SecundTVCell
        if MainData.sharedInstance.result.count > 0 {
           cell.myImageView.af_setImage(withURL: URL(string: MainData.sharedInstance.result[0].partners[indexPath.row].img)!)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "FromThird", sender: MainData.sharedInstance.result[0].partners[indexPath.row].link)
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
      
        if  segue.identifier == "FromThird" {
            if let dVC = segue.destination as?  ThirdVC{
                dVC.link = (sender as? String)!
            }
        }
//        if let selectedPartner = segue.destination as? ThirdVC {
//            let indexPath = self.tableView.indexPathForSelectedRow
//            let weblink = MainData.sharedInstance.result[0].partners[(indexPath?.row)!].link
//            selectedPartner.link = weblink
//        }
//    }
 
    
}
}
